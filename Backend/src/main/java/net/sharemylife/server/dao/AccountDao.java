/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.dao;

import net.sharemylife.server.models.Account;


/**
 *
 * @author dkozyrev
 */
public interface AccountDao extends GenericDao<Account, Long> {

    public void addAccount(Account model);
	public Account getAccountByLogin(String login);
}
