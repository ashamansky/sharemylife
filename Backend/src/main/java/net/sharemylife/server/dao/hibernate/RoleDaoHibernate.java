/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.dao.hibernate;

import net.sharemylife.server.dao.RoleDao;
import net.sharemylife.server.models.Account;
import net.sharemylife.server.models.Role;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author dkozyrev
 */
@Repository("roleDao")
public class RoleDaoHibernate extends GenericDaoHibernate<Role, Long> implements RoleDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    public RoleDaoHibernate(@Qualifier("sessionFactory") SessionFactory sessionFactory)
    {
        super(Role.class);
        setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional
    public void addRole(Role role) {
        getHibernateTemplateModified().saveOrUpdate(role);
    }

    @Override
    public Role getByAccountAndRole(Account account, String role) {


        List<Role> res = (List<Role>)this.getHibernateTemplate().find(
                "from " + Role.class.getName()
                + " obj where obj.acc = ? and obj.roleValue = ?",
                account, role);
        if (res.isEmpty()) {
            return null;
        }

        return res.get(0);
    }

	@Override
	public List<Role> getByAccount(Account account) {
        List<Role> res = (List<Role>)this.getHibernateTemplate()
				.find( "from " + Role.class.getName()
                + " obj where obj.acc = ?", account);
        if (res.isEmpty()) {
            return null;
        }
        return res;
	}
}
