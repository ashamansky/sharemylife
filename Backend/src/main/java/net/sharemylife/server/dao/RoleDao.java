/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.dao;

import net.sharemylife.server.models.Account;
import net.sharemylife.server.models.Role;
import java.util.List;

/**
 *
 * @author dkozyrev
 */
public interface RoleDao extends GenericDao<Role, Long> {

    public void addRole(Role role);

    public Role getByAccountAndRole(Account account, String string);
	
	public List<Role> getByAccount(Account account);
}
