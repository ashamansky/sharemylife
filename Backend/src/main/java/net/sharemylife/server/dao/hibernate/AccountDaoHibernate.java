/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.dao.hibernate;

import net.sharemylife.server.dao.AccountDao;
import net.sharemylife.server.models.Account;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author dkozyrev
 */
@Repository("userDao")
public class AccountDaoHibernate extends GenericDaoHibernate<Account, Long> implements AccountDao
{

	@Autowired
    public AccountDaoHibernate(@Qualifier("sessionFactory") SessionFactory sessionFactory)
    {
        super(Account.class);
        setSessionFactory(sessionFactory);
    }

    @Override
    public void addAccount(Account model) {
        this.getHibernateTemplateModified().saveOrUpdate(model);
    }

    @Override
	public Account getAccountByLogin(String login) {
            List<Account> res = (List<Account>)this.getHibernateTemplate().find("from " + Account.class.getName() + " obj where obj.email=?", login);
            if (res.isEmpty()) {
                return null;
            } else {
                return res.get(0);
            }
	}

}
