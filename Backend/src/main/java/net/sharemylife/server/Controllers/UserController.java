package net.sharemylife.server.Controllers;

import net.sharemylife.server.models.Account;
import net.sharemylife.server.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexander on 10.10.14.
 */

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    @Qualifier("accountValidator")
    private Validator validator;

    @Autowired
    AccountService accountService;

    @InitBinder
    public void binder(WebDataBinder binder) {
        binder.addValidators(validator);
    }

    @RequestMapping(value="/", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public Account createAccount(@RequestBody @Validated Account account, BindingResult result) {
        if (!result.hasErrors()) {
            accountService.saveAccount(account);
        }
        return account;
    }

    @RequestMapping(value="/", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public List<Account> getAccounts() {
        return accountService.getAll();
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE, produces="application/json")
    @ResponseBody
    public Map<String, String> deleteAccount(@PathVariable(value = "id") Long id) {
        Map<String, String> result = new HashMap<String, String>();
        if  (accountService.exists(id)) {
            accountService.remove(id);
            result.put("status", "OK");
        } else {
            result.put("status", "User wasn't found");
        }
        return result;
    }



}
