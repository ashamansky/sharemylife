package net.sharemylife.server.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexander on 13.10.14.
 */
@RestController
@RequestMapping(value = "/")
public class DefaultController {

    @RequestMapping(value = "/")
    public Map<String, List<String>> indexHandler() {

        Map<String, List<String>> results = new HashMap<String, List<String>>();
        Class t = UserController.class;
        Method[] m = t.getDeclaredMethods();
        List<String> methods = new ArrayList<String>(m.length);
        for(Method method : m) {
            methods.add(method.toGenericString());
        }
        results.put("UserController", methods);

        return results;
    }
}
