/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.services.impl;

import net.sharemylife.server.dao.RoleDao;
import net.sharemylife.server.models.Account;
import net.sharemylife.server.models.Role;
import net.sharemylife.server.services.RoleService;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * @author admin
 */
@Repository("roleService")
public class RoleServiceImpl implements RoleService{
	
	@Resource(name="roleDao")
	RoleDao roleDao;

	@Override
	public Role getByAccountAndRole(Account account, String string) {
		return roleDao.getByAccountAndRole(account, string);
	}

	@Override
	public List<Role> getByAccount(Account account) {
		return roleDao.getByAccount(account);
	}

	@Override
	public Role save(Role model) {
		return roleDao.save(model);
	}

	@Override
	public Role get(Long id) {
		return roleDao.get(id);
	}

	@Override
	public void remove(Long id) {
		roleDao.remove(id);
	}

	@Override
	public List<Role> getAll() {
		return roleDao.getAll();
	}

    @Override
    public boolean exists(Long id) {
        return roleDao.exists(id);
    }

}
