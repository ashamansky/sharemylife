/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.sharemylife.server.services;

/**
 *
 * @author zedd
 */
public interface CryptService
{
	public String cryptPassword(String password, String salt);
	public String cryptCookiesPassword(String login, String passwordHash);
	public String getSalt(String passwordHash);
    public String md5(String data);
}
