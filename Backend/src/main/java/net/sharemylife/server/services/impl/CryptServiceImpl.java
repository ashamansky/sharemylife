/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.sharemylife.server.services.impl;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import net.sharemylife.server.services.CryptService;
import org.springframework.stereotype.Repository;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 *
 * @author zedd
 */
@Repository("cryptService")
public class CryptServiceImpl implements CryptService
{
	private final static int saltLength = 5;

    @Override
    public String md5(String data) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(data.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

	@Override
	public String cryptPassword(String password, String salt)
	{
		return generateHash(password, salt);
	}

	private static String generateHash(String text, String salt)
	{
		MessageDigest m, sha;
		byte[] saltb;
		if(salt != null)saltb = salt.getBytes();
		byte[] textb = text.getBytes();
		String finalStr = "";

		try
		{
			m = MessageDigest.getInstance("MD5");
			sha = MessageDigest.getInstance("SHA-1");

			if (salt == null)
			{
				m.update(UUID.randomUUID().toString().getBytes());
				saltb = m.digest();
                saltb = Base64.encode(saltb).substring(0, saltLength).getBytes();
			} else {
				saltb = salt.getBytes();
			}

			byte[] res = new byte[saltLength + text.length()];
			for(int x = 0; x < res.length; x++)
			{
				if(x < saltLength)
				{
					res[x] = saltb[x];
				}else
				{
					res[x] = textb[x - saltLength];
				}
			}
			sha.update(res);
			finalStr = Base64.encode(sha.digest());
			finalStr = (new String(saltb)) + finalStr;
			finalStr = finalStr.replace("\n", "");
			finalStr = finalStr.replace("\r", "");
			return  finalStr;
		} catch (NoSuchAlgorithmException ex) {
//			Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

	@Override
	public String getSalt(String passwordHash)
	{
		if ( null != passwordHash && !passwordHash.isEmpty() )
		{
			return passwordHash.substring(0, saltLength);
		}else
		{
			return null;
		}
	}

	@Override
	public String cryptCookiesPassword(String login, String passwordHash)
	{

		byte[] pass;
		MessageDigest m;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException ex) 
		{
			return null;
		}
		m.reset();
		m.update(login.getBytes());
		m.update(":".getBytes());

		if ( null == passwordHash || passwordHash.isEmpty())
			return null;

		m.update(passwordHash.getBytes());
		pass = m.digest();
		pass = Base64.encode(pass).substring(0, saltLength).getBytes();
		return new String(pass);
	}

}
