/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.services.impl;

import net.sharemylife.server.dao.AccountDao;
import net.sharemylife.server.dao.RoleDao;
import net.sharemylife.server.models.Account;
import net.sharemylife.server.models.Role;
import net.sharemylife.server.services.AccountService;
import net.sharemylife.server.services.CryptService;
import net.sharemylife.server.services.RoleService;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author admin
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService{
	
	@Resource(name = "userDao")
	private AccountDao accountDao;

    @Resource(name = "cryptService")
    private CryptService cryptService;
	
	@Resource(name = "roleService")
	private RoleService roleService;
	
	@Resource(name="roleDao")
	RoleDao roleDao;

	@Override
	public Account save(Account model) {
		return accountDao.save(model);
	}

	@Override
	public Account get(Long id) {
		return accountDao.get(id);
	}

	@Override
	public void remove(Long id) {
		accountDao.remove(id);
	}

	@Override
	public List<Account> getAll() {
		return accountDao.getAll();
	}

    @Override
    public boolean exists(Long id) {
        return accountDao.exists(id);
    }

    @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		return getAccountByLogin(username);
	}

	@Override
	public Account setAuthentication(String user, String password, Collection<? extends GrantedAuthority> authorities) {
		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken(user, password, authorities));
		return null;
	}

	@Override
	public Account setAuthentication(String user, String password, Collection<? extends GrantedAuthority> authorities, HttpServletResponse httpServletResponse) {
		
		setAuthentication(user, password, authorities);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return null;
	}

	@Override
	public Account getAccountByLogin(String login) {
		Account account = accountDao.getAccountByLogin(login);
		return account;
	}
	
	@Override
	public Account getLoggedInAccount() {
			Account account = null;

		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = "";
			
			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			} else {
				username = principal.toString();
			}

			account = this.getAccountByLogin(username);

			Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)SecurityContextHolder.getContext().getAuthentication().getAuthorities();

			System.out.println(authorities.toString());

			return account;
		} catch (ClassCastException cce) {
			return (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (NullPointerException ex) {
			return null;
		}
	}
	
	@Override
	public boolean hasRole(ArrayList<String> roles) {
		try {
			Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)SecurityContextHolder.getContext().getAuthentication().getAuthorities();

			for (GrantedAuthority auth : authorities) {
				for (String role : roles) {
					if (auth.getAuthority().equals(role)) {
						return true;
					}
				}
			}
		} catch (NullPointerException e) {
		}

		return false;
	}

	@Override
	public void setRole(Account account, String roleName) {
				
		//remove old roles
		List<Role> oroles = roleService.getByAccount(account);
		for (Role orole : oroles) {
			roleService.remove(orole.getId());
		}

		//add new role
		Role role = new Role();
		role.setRoleValue(roleName);
		role.setAcc(account);
		role = roleService.save(role);

		//add role to account
		Set<Role> roles = new HashSet<Role>();
		roles.add(role);
		account.setRoles(roles);
		save(account);
	}

	@Override
	public String getRole() {
				try {
			List<GrantedAuthority> authorities = (List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
			if (authorities.isEmpty()) {
				return "ROLE_ANONYMOUS";
			} else {
				return authorities.get(0).getAuthority();
			}
		} catch (Exception ex) {
			return "ROLE_ANONYMOUS";
		}
	}


    @Override
    @Transactional
    public Account saveAccount(Account model) {
        try {


            String salt = cryptService.getSalt(cryptService.md5(model.getEmail()));

            model.setPassword(cryptService.cryptPassword(model.getPassword(), salt));

            accountDao.addAccount(model);
            Role newRole = new Role();
            newRole.setRoleValue(RoleService.DEFAULT_ROLE);
            newRole.setAcc(model);
            roleDao.addRole(newRole);

        } catch (Exception e) {
            System.out.println(e);
            System.out.println(e.getMessage());
        } finally {
            return model;
        }
    }
	
	@Override
	public void deleteAccount(Account account) {
		accountDao.remove(account.getId());
	}
	
	@Override
	public boolean hasOneRole(String role) {
			ArrayList<String> roles = new ArrayList<String>();
		roles.add(role);
		return hasRole(roles);
	}
        
        @Override
	public void logout() {
		SecurityContextHolder.getContext().setAuthentication(null);
	}
	
}
