/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.services;

import net.sharemylife.server.models.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author admin
 */
public interface AccountService extends GenericService<Account, Long>, UserDetailsService {



	public Account setAuthentication(String user, String password, Collection<? extends GrantedAuthority> authorities);

	public Account setAuthentication(String user, String password, Collection<? extends GrantedAuthority> authorities, HttpServletResponse httpServletResponse);
	
	public Account getLoggedInAccount();
		public boolean hasOneRole(String role);

	public void setRole(Account account, String roleName);

	public String getRole();
	
	public Account saveAccount(Account model);
		
	public void deleteAccount(Account account);
	public Account getAccountByLogin(String login);
	public boolean hasRole(ArrayList<String> roles);
    public void logout();
}
