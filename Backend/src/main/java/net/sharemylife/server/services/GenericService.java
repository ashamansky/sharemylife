/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.services;

import java.util.List;

/**
 *
 * @author dbukhanets
 */
public interface GenericService <T, K>
{
	public T save(T model);
	public T get(K id);
	public void remove(K id);
	public List<T> getAll();
    public boolean exists(K id);
}
