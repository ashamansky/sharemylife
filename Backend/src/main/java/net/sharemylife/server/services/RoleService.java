/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.services;

import net.sharemylife.server.models.Account;
import net.sharemylife.server.models.Role;
import java.util.List;

/**
 *
 * @author dbukhanets
 */
public interface RoleService extends GenericService<Role, Long> {
    public static String DEFAULT_ROLE = "ROLE_USER";

    public Role getByAccountAndRole(Account account, String string);
	
	public List<Role> getByAccount(Account account);
	
}
