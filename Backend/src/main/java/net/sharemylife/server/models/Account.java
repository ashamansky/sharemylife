/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sharemylife.server.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Entity
@Table(name="tbl_account")
public class Account implements Serializable, UserDetails {

	@Id
    @GeneratedValue
    @Column(name="id", nullable=false)
	private Long id;
	
    @Column(name="firstName", length=128, nullable=true)
	private String firstName;
	
    @Column(name="lastName", length=128, nullable=true)
	private String lastName;
	
    @Column(name="zip", length=128, nullable=true)
	private String zip;
	
    @Column(name="gender", length=128, nullable=true)
	private String gender;
	
    @Column(name="birthday", length=128, nullable=true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date birthday;
	
    @Column(name="email", length=128, nullable=false)
	private String email;

    @JsonIgnore
    @Column(name="password", length=128, nullable=true)
	private String password;

    @JsonIgnore
    private String retypePassword;

    @Column(name="auth_type", length=128, nullable = true)
    private String authType;

    @Column(name = "auth_token", length = 512, nullable = true)
    private String authToken;

	@OneToMany(cascade = {CascadeType.ALL},
        fetch = FetchType.EAGER, mappedBy = "acc")
        private Set<Role> roles;


	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Account() {
	}
		
	public Date getBirthday() {
		return birthday;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getGender() {
		return gender;
	}

	public Long getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPassword() {
		return password;
	}

	public String getZip() {
		return zip;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

    public String getAuthType() {return authType;}

    public void setAuthType(String authType) {this.authType = authType;}

    public String getAuthToken() {return authToken;}

    public void setAuthToken(String authToken) {this.authToken = authToken;}

    public String getRetypePassword() {return retypePassword;}

    public void setRetypePassword(String retypePassword) {this.retypePassword = retypePassword;}

    @JsonIgnore
    @Override
	public Collection<GrantedAuthority> getAuthorities() {
		  Collection<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
      for (Role role : roles) {
          list.add(new GrantedAuthorityImpl(role.getRoleValue()));
      }
      return list;
	}

        @JsonIgnore
        @Override
        public String getUsername() {
                return email;
        }

        @JsonIgnore
        @Override
        public boolean isAccountNonExpired() {
                return true;
        }

        @JsonIgnore
        @Override
        public boolean isAccountNonLocked() {
                return true;
        }

        @JsonIgnore
        @Override
        public boolean isCredentialsNonExpired() {
                return true;
        }

        @JsonIgnore
        @Override
        public boolean isEnabled() {
                return true;
        }
  
}
